#include <xmmintrin.h>
#include <math.h>
#include <stdio.h>

int main(void)
{
	const int length = 64000;
	printf("Calculating %d, values...\n", length);

	float* pResult = (float*)_aligned_malloc(length * sizeof(float), 16);

	__m128 x;
	__m128 xDelta = _mm_set1_ps(4.0f);
	__m128* pResultSSE = (__m128*) pResult;

	const int SSELength = length / 4;
	
	for (int i = 0; i < 100000; ++i)
	{
		x = _mm_set_ps(4.0f, 3.0f, 2.0f, 1.0f);
		for (int j = 0; j < SSELength; ++j)
		{
			__m128 xSqrt = _mm_sqrt_ps(x);

			__m128 xRecip = _mm_rcp_ps(x);

			pResultSSE[j] = _mm_mul_ps(xRecip, xSqrt);

			x = _mm_add_ps(x, xDelta);
		}

		float xFloat = 1.0f;
		for (int j = 0; j < length; ++j)
		{
			pResult[j] = sqrt(xFloat) / xFloat;
			xFloat += 1.0f;
		}
	}
	
	for (int i = 0; i < 20; i++)
	{

		printf("Result[%d] = %f\n", i, pResult[i]);
	}

	_aligned_free(pResult);

	return 0;
}